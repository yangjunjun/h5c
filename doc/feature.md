# h5编辑工具 - 功能

## h5信息

  title: '示例作品',
  description: "摘要",
  icon: '',
  music: {
    audioURL: '/static/audios/background.mp3',
    imgURL: '/static/img/music.png',
    enable: false
  },
  pages: [{
    title: '第一个页面'
    enable: 'true',
    background-color: '#fff',
    backgroundImage: 'none',
    comps: []
  }]
  
## 全局功能

导航面板， 编辑面板， 工具面板 均可拖曳，并能保存位置信息(todo)
导航面板， 工具面板 均可缩放，并能保存大小信息(todo)
拖曳功能支持 键盘 (todo)( todo )

## 模版功能

1. h5 整体模版
2. h5 页面模版 

## 顶部面板 (headerZone)

1. 保存 ( todo )
2. 预览 ( todo )

## 导航面板 ( layerZone )

** 页面tab **

0. 默认有一个空白页面
1. 新增 ( 新增切换模版tab, todo)
2. 复制
3. 删除 (最后一张不可删除)
4. 改变顺序(拖动改变顺序, todo)
5. 样式 (缩略图, 可滚动)

** 模版tab ** ( todo )

内容模版列表，第一个为空白模版, 其他为系统内置的页面模版
选择"新建页面" 按钮, b切换至"模版Tab", 
选择任意"模版", 切换至"页面Tab", 并且默认选择新增的"页面"


## 工具面板 ( toolZone )

1. 样式（增加 tooltip, todo） 


## 编辑面板 ( canvasZone )

1. 预览页面 ( todo )
2. 撤销 ( todo )
3. 重做 ( todo )

## 组件显示面板 ( canvasZone )

** 文字组件 **

** 图片组件 **

** 音频组件 **

** 表单组件 **

** 视频组件 **

** 其他  **

## 组件编辑面板 ( editZone )



##  data 生成 h5 页面功能


## 插件

1. drag 提供 drag resize 功能
2. tab 提供页面布局功能
3. slider 提供数值输入功能
4. dialog 提供弹窗功能
5. colorPicker 提供颜色选择功能

todo 
=============================================

1. 增加组件种类
2. 界面优化
3. 增加提示
4. 组件化
5. 使用 vuex 改造




