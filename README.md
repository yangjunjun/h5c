# h5c

> online H5 creator （开发交流群 153447214）

## 使用

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:4001
npm run dev

# build for production with minification
npm run build
```

## 界面预览

![](h5-editor.png)