if( process.env.NODE_ENV !== 'production') {
  var host = 'http://localhost:4001';
}
else {
  var host = '';
}
window.Global = {
  api: {
    h5: '/api/h5',
    picture: '/api/picture',
    music: '/api/music',
    h5pc: host + "/slider/pc.html#",
    h5wap: host + "/slider/wap.html#"
  },
  isAdmin: false
}

window.GetUrlParms = function () {
  var args = {};
  var query = location.search.substring(1); 
  var pairs = query.split("&"); 
  for (var i = 0; i < pairs.length; i++) {
    var pos = pairs[i].indexOf('='); 
    if (pos == -1) continue;
    var argname = pairs[i].substring(0, pos); 
    var value = pairs[i].substring(pos + 1);
    args[argname] = unescape(value); 
  }
  return args;
} 
var query  = window.GetUrlParms();
if(query['role']== 'admin') {
  window.Global.isAdmin = true;
}

var Vue = require('vue'); 
var App = require('./app.vue');

Vue.config.debug = true;
Vue.config.devtools = true;

Vue.filter('realImgSrc', function (src) {
  return host + src;
});

Vue.filter('pageStyleFix', function (obj) {
  var tmp = {};
  for(var prop in obj){
    if(['backgroundImage'].indexOf(prop) > -1){
      if(obj[prop]){
        tmp[prop] = 'url(' + Global.api.basePicture + obj[prop] + ')';
      }
    }
    else{
      tmp[prop] =obj[prop]
    }
  }
  return tmp;
});

Vue.directive('validate', {
  params: ['min', 'max'],
  twoWay: true,
  bind: function () {
  },
  update: function (result) {
    var min = this.params.min;
    var max = this.params.max;

    this.handler = function () {
      var value = parseFloat(this.el.value);

      if(isNaN(value)) {
        value = 0;
      }
      else if(value < min) {
        value = min;
      }
      else if (value >= max){
        value = max
      }
      else{
        value = this.el.value;
      }
      this.el.value = value;
      this.set(parseFloat(value));
    }.bind(this)
    this.el.addEventListener('input', this.handler);
  }
});

new Vue({
  el:'body',
  components:{
    app: App
  }  
});



//  防止按 backspace 键使浏览器后退
$(document).on('keydown', 'input, textarea', function(event){
  event.stopPropagation();
});

$(document).on('keydown', function(e){
  if(e.keyCode === 8){
    return false;
  }
});

